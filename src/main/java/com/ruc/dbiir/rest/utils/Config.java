package com.ruc.dbiir.rest.utils;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class Config {

	/**
	 *   //add()和remove()方法在失败的时候会抛出异常(不推荐)
        //添加元素
        queue.offer("a");
        for(String q : queue){
            System.out.println(q);
        }
        System.out.println("poll="+queue.poll()); //返回第一个元素，并在队列中删除
        System.out.println("element="+queue.element()); //返回第一个元素 
        System.out.println("peek="+queue.peek()); //返回第一个元素 
	 */
	public static Queue<String> NUMQUEUE = new LinkedList<String>();
	public static Queue<String> ERRORQUEUE = new LinkedList<String>();
	public static Deque<String> WATERQUEUE = new ArrayDeque<String>();
	
	public static HashSet<String> ERRTABLE = new HashSet<String>();

	public static String TOPIC_RATE = "newrate";
	public static String TOPIC_ERRORWATER = "errorWater";
	public static String TOPIC_WATER = "output";
	public static String TOPIC_AGG = "aggregate";

	public static String BROKER = "localhost:2181"; 

	/**
	 * 通过差值来进行数据生成速率的度量
	 */
	public static int PRE_TUPLES = 0;
	
	public static String PRE_KEY = "initial";
	public static int ACC_VALUE = 0;
	
	/**
	 * 服务器相关信息
	 */

//	public static String IP = "202.112.113.71";
//	public static String USER = "rucer";
//	public static String PASSWD  = "rucer2017";

//	public static String IP = "100.115.142.160";
//	public static String USER = "root";
//	public static String PASSWD  = "TDSQL@test";
	
	
	public static String IP = "10.77.50.50";
	public static String USER = "wtt";
	public static String PASSWD  = "123456";
	
	
	public static String CMD_STORM_EXE_PARAMS = "bash /home/wtt/markcheng/shell/storm_exe_params.sh ";
	public static String CMD_STORM_KILL = "bash /home/wtt/markcheng/shell/killjob.sh ";
	

}
