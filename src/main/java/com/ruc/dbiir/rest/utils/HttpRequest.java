package com.ruc.dbiir.rest.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

public class HttpRequest {



	public static void httpGetMeth(String url) {
		
		try {
			//创建Http Request(内部使用HttpURLConnection)    
			ClientHttpRequest request =     
					new SimpleClientHttpRequestFactory().       
					createRequest(new URI(url), HttpMethod.GET);  
			ClientHttpResponse response = request.execute();     
			if (response.getStatusCode()==HttpStatus.OK) {  
				System.out.println("Response ok");  
			}  
			InputStream is = response.getBody();  
			byte bytes[] = new byte[(int)response.getBody().available()];    
			is.read(bytes);    
			//			System.out.println(bytes.toString());
			String resultStr = new String(bytes, Charset.forName("utf-8"));    
			System.out.println(resultStr);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}  
	}

	// test
	public static void main(String[] args) {
		String url = "http://202.112.113.71:8181/getCompany?content=beijin";
		httpGetMeth(url);
	}

}
