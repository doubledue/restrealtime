package com.ruc.dbiir.rest.datasource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.ruc.dbiir.rest.utils.Config;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.serializer.StringDecoder;
import kafka.utils.VerifiableProperties;

/**
 * 从kafka消息队列读（消费）数据
 * author：mark   
 * createTime：May 26, 2018 1:27:41 PM   
 * @version
 */


//@Component(value = "readKafka")
public class AggConsumer {
	
	private final ConsumerConnector consumer;
	
	public AggConsumer(String broker) {
		Properties props = new Properties();
		//zookeeper 配置
		props.put("zookeeper.connect", broker);
		//group 代表一个消费组
		props.put("group.id", "jd-group");
		//zk连接超时
		props.put("zookeeper.session.timeout.ms", "4000000");
		props.put("zookeeper.sync.time.ms", "200");
		props.put("auto.commit.interval.ms", "1000");
		props.put("auto.offset.reset", "smallest");
		//序列化类
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		ConsumerConfig config = new ConsumerConfig(props);
		consumer = kafka.consumer.Consumer.createJavaConsumerConnector(config);
		
	}


	public void consume() {
		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
		topicCountMap.put(Config.TOPIC_AGG, new Integer(1));
		
		StringDecoder keyDecoder = new StringDecoder(new VerifiableProperties());
		StringDecoder valueDecoder = new StringDecoder(new VerifiableProperties());

		Map<String, List<KafkaStream<String, String>>> consumerMap = 
				consumer.createMessageStreams(topicCountMap,keyDecoder,valueDecoder);
		
		//消费aggregate数据
		KafkaStream<String, String> aggStream = consumerMap.get(Config.TOPIC_AGG).get(0);
		ConsumerIterator<String, String> aggIt = aggStream.iterator();
		while (aggIt.hasNext()) {
			String aggInfo = aggIt.next().message();
			Config.WATERQUEUE.push(aggInfo);
			System.out.println(Config.WATERQUEUE.size());
		}
	}

	public static void main(String[] args)
	{
//		new KafkaConsumer().consume();
	}
}
