package com.ruc.dbiir.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruc.dbiir.rest.datasource.AggConsumer;
import com.ruc.dbiir.rest.datasource.ErrorConsumer;
import com.ruc.dbiir.rest.utils.Config;
import com.ruc.dbiir.rest.utils.HttpUtil;

@Controller
@RequestMapping(value = "/dashboard")
public class DashboardController
{
	public static ErrorConsumer error = null;
	public static AggConsumer aggregate = null;
	
	@ResponseBody
	@RequestMapping(value = "/getTopologyInfo")
	public Object getTopologyInfo()
	{
		String url = "http://100.115.142.160:8080/api/v1/topology/generator-2-1529323135";
		String result = HttpUtil.doGet(url);
		return result;
	}

	@RequestMapping(value = "/startConsumer")
	public void startConsumer()
	{
		new Thread(new Runnable()
		{
			public void run()
			{
				error = new ErrorConsumer(Config.BROKER);
				System.out.println("errorConsumer has been started!");
				error.consume();
			}
		}).run();
	}
	@RequestMapping(value = "/startAgg")
	public void startAgg()
	{
		new Thread(new Runnable()
		{
			public void run()
			{
				aggregate = new AggConsumer(Config.BROKER);
				System.out.println("aggregate has been started!");
				aggregate.consume();
			}
		}).run();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getError")
	public Object getError()
	{
		List<String> result = new ArrayList<String>();
		for(String value : Config.ERRTABLE){  
			// System.out.println(value);
            result.add(value);
        }
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getInfo")
	public Object getInfo()
	{
		String aggInfo = Config.WATERQUEUE.peek();
		if (aggInfo == null)
		{
			aggInfo = "{\"totalcount\":0,\"errorcount\":0,\"totalamt\":0,\"maxamt\":0}";
		}
		return aggInfo;
	}
	
}
